# CrasherBotty

A Twitchbot for iRacing streamers. On first launch, the application will create a 'crasherbot-config.json' file in the same directory. Please use a text editor to change the values. Note a seperate bot account is assumed.

## Modules

### Marbles

Manage an AI roster and play marbles using iRacing.

-   !race/!play : to join an open registration
-   !marbles : info text

#### Broadcaster

-   !marbles [rosterName] : Start a registration using an existing iRacing roster

### Custom Player

Change and update player data. Currently this module is only available for broadcasters.

#### Broadcaster

-   !name [name] : Change your iRacing display name
-   !number [number] : Change your iRacing car number
-   !color [colorName] : Update your cars main color using a css color name

### Witty

The witty module adds simple responses to various basic commands. Try some!

### Timer

The timer module allows to set a timer, for things.

-   !timer : Get the remaining timer time

#### Moderators

-   !timer [time in seconds] : Set a new timer (if none running)
-   !timer reset : Reset the timer to the initial amount
