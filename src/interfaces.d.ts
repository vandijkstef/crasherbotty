export interface DesignData {
	designId: number;
	baseColor: string;
	accentColor: string;
	highLight: string;
}

export interface CarDesign extends DesignData {}

export interface SuitDesign extends DesignData {}

export interface HelmetDesign extends DesignData {}

export interface NumberDesign {}

export interface Driver {
	twitchName: string;
	driverName: string;
	twitchId: string;
	irId: number;
	number: number;
	carDesign: CarDesign;
	suitDesign: SuitDesign;
	helmetDesign: HelmetDesign;
	cache?: {
		badges?: tmi.Badges;
	};
}

export interface RaceOptions {
	numberDesign: string;
	carPath: string;
	carId: number;
}

export interface Car {
	path: string;
	id: number;
}

export interface OutputDriver {
	driverName: string;
	carDesign: string;
	carNumber: number;
	suitDesign: string;
	helmetDesign: string;
	carPath: string;
	carId: number;
	sponsor1: number;
	sponsor2: number;
	numberDesign: string;
	driverSkill: number;
	driverAggression: number;
	driverOptimism: number;
	driverSmoothness: number;
	pitCrewSkill: number;
	strategyRiskiness: number;
	driverAge: number;
	id: string;
	rowIndex: number;
	carClassId: number;
}

export interface Output {
	drivers: OutputDriver[];
}
