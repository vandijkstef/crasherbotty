// @ts-ignore
import * as csscolors from 'css-color-names';
import * as tmi from 'tmi.js';

import { Player } from '../classes/player';
import { CrasherModule } from './crasherModule';

const helloArray = [
	"Yea I'm here",
	"It's me",
	"Is it me you're looking for?",
	'Hello, Hello, Hello',
	'HeyGuys',
];

const lurkArray = ['Bye, have a great time!'];

const neverArray = [
	'... gonna give you up',
	'... gonna let you down',
	'... gonna run around and desert you',
	'... gonna make you cry',
	'... gonna say goodbye',
	'... gonna tell a lie and hurt you',
];

export class Witty extends CrasherModule {
	private helloIndex: number;
	private lurkIndex: number;
	private neverIndex: number;

	constructor(client: tmi.Client) {
		super(client);

		this.helloIndex = 0;
		this.lurkIndex = 0;
		this.neverIndex = 0;
	}

	protected chat(command: string, player: Player, messageData: string[]) {
		switch (command) {
			case 'hello':
				this.say(helloArray[this.helloIndex]);
				this.helloIndex++;
				if (this.helloIndex == helloArray.length) {
					this.helloIndex = 0;
				}
				return;

			case 'bot':
				this.say("I'm the CrasherBot!");
				return;

			case 'lurk':
				this.say(lurkArray[this.lurkIndex]);
				this.lurkIndex++;
				if (this.lurkIndex == lurkArray.length) {
					this.lurkIndex = 0;
				}
				return;
				return;

			case 'ping':
				this.say('Poink!');
				return;

			case 'weather':
			case 'rain':
				this.say("It's likely not raining");
				return;

			case 'punt':
			case 'punted':
			case 'punterino':
				this.say('Jimmer, please...');
				return;

			case 'never':
				this.say(neverArray[this.neverIndex]);
				this.neverIndex++;
				if (this.neverIndex == neverArray.length) {
					this.neverIndex = 0;
				}
				return;

			case 'max':
				this.say('dogJAM Super.. Super.. dogJAM');
				return;

			case 'lewis':
				this.say('Get in there!');
				return;
		}
	}
}
