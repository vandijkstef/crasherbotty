import * as tmi from 'tmi.js';

import { Player } from '../classes/player';
import { CrasherModule } from './crasherModule';

export class Timer extends CrasherModule {
	private timer: number;
	private resetTime: number;

	constructor(client: tmi.Client) {
		super(client);

		this.timer = -1;
		this.resetTime = 0;

		setInterval(() => {
			this.timer = this.timer - 1;

			if (this.timer == 0) {
				this.say('Timer is done!');
			}

			if (this.timer < -1) {
				this.timer = -1;
			}
		}, 1000);
	}

	protected chat(command: string, player: Player, messageData: string[]) {
		switch (command) {
			case 'timer':
				const arg = messageData[1];
				if (!arg) {
					if (this.timer > 0) {
						this.say(
							`There are ${this.timer} seconds left on the timer`,
						);
						return;
					}

					this.say(`There is no timer`);
					return;
				}

				if (
					player.data.twitchName.toLowerCase() !== 'crashervandy' ||
					player.data.cache?.badges.moderator
				) {
					return;
				}

				if (!isNaN(parseInt(arg, 10))) {
					if (this.timer > 0) {
						this.say(`Timer already running`);
						return;
					}

					this.timer = parseInt(arg, 10);
					this.resetTime = this.timer;
					this.say(`Timer started for ${this.timer} seconds`);
					return;
				}

				if (arg === 'reset') {
					this.say(`Timer reset`);
					this.timer = this.resetTime;
					return;
				}
				
				return;
		}
	}
}
