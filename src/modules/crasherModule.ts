import chalk from 'chalk';
import * as tmi from 'tmi.js';

import { Player } from '../classes/player';

export class CrasherModule {
	protected client: tmi.Client;
	protected channel: string;

	constructor(client: tmi.Client) {
		this.client = client;
		this.channel = '';

		this.client.on('message', this.handleChat);

		console.log(
			`${chalk.bgGreen.black(`Loaded CrasherModule`)} ${
				this.constructor.name
			}`,
		);
	}

	private handleChat = (
		channel: string,
		tags: tmi.ChatUserstate,
		message: string,
		self: boolean,
	) => {
		if (!this.channel) this.channel = channel;
		if (self) return;
		if (message.charAt(0) !== '!') return;

		const player = new Player(tags);
		const messageData = message.substring(1).split(' ');
		const command = messageData[0].toLowerCase();

		const handledAsAdmin =
			player.badges?.broadcaster && this.chatAdmin(command, messageData);

		if (!handledAsAdmin) {
			this.chat(command, player, messageData);
		}
	};

	protected chatAdmin(command: string, messageData: string[]): boolean {
		return false;
	}

	protected chat(
		command: string,
		player: Player,
		messageData: string[],
	): void {
		console.log(`Chat not implemented for: ${this.constructor.name}`);
	}

	protected say(message: string): void {
		this.client.say(this.channel, message);
	}
}
